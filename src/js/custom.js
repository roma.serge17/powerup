// Burger Menu

let menuBtn = document.querySelector('.menu-btn');
let menu = document.querySelector('.menu-burger');
menuBtn.addEventListener('click', function () {
    menuBtn.classList.toggle('active');
    menu.classList.toggle('active');
})


// Webbutik JS

const btnWorksSection = document.querySelector('.webbutik_box-card .btn.load-shop')
btnWorksSection.addEventListener('click', function (e) {
    e.preventDefault()
    setTimeout(showCards, 100)
})
function showCards() {
    const cards = document.querySelectorAll('.card')
    cards.forEach(elem => elem.classList.add('active', 'visible'))
}
function showLoader(selector) {
    document.querySelector(selector).style.display = 'block'
}
function hideLoader(selector) {
    document.querySelector(selector).style.display = 'none'
}

//Form Label JQ *

$('#form_name').focus(function () {
    $('#hide-name').hide();
});
$('#form_name').blur(function () {
    if ($(this).val().trim() === '') {
        $('#hide-name').show();
    }
});
$('#form_email').focus(function () {
    $('#hide-email').hide();
});

$('#form_email').blur(function () {
    if ($(this).val().trim() === '') {
        $('#hide-email').show();
    }
});




